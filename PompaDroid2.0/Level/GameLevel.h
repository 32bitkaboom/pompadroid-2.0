//
//  GameLevel.h
//  PompaDroid2.0
//
//  Created by John Watson on 6/29/14.
//  Copyright (c) 2014 John Watson. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

typedef enum : NSUInteger {
    PDGameLevel1,
    PDGameLevel2,
    PDGameLevel3,
} PDGameLevel;

@interface GameLevel : SKScene

- (void) loadLevel:(PDGameLevel)level;

@end
