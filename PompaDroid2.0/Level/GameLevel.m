//
//  GameLevel.m
//  PompaDroid2.0
//
//  Created by John Watson on 6/29/14.
//  Copyright (c) 2014 John Watson. All rights reserved.
//

#import "GameLevel.h"
#import "JSTileMap.h"

@interface GameLevel ()

@property (nonatomic, strong) JSTileMap *tileMap;
@property (nonatomic, strong) SKNode *worldNode;

@end

@implementation GameLevel

-(instancetype)initWithSize:(CGSize)size
{
    if( self = [super initWithSize:size] )
    {
        _worldNode= [[SKNode alloc] init];
        [self addChild:_worldNode];
        
        
    }
    
    return self;
}

//-(void)didMoveToView:(SKView *)view
//{
//    UIGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
//    [view addGestureRecognizer:tapGesture];
//}

-(void)loadLevel:(PDGameLevel)level
{
    switch (level) {
        case PDGameLevel1:
            [self loadMapNamed:@"pd_tilemap.tmx"];
            break;
            
        default:
            break;
    }
}

- (void) loadMapNamed:(NSString *)mapName
{
    self.tileMap = [JSTileMap mapNamed:mapName];
    if( self.tileMap )
    {
        self.tileMap.position = CGPointMake(0, 0);
        [self addChild:self.tileMap];
    }
    
    }

//- (void) handleTap:(UIGestureRecognizer*)gestureRecognize
//{
//    NSLog(@"LEVEL TAPPED");
//}

-(void)update:(NSTimeInterval)currentTime
{
    
}

@end
