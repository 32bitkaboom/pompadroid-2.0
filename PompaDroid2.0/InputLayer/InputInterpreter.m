//
//  InputInterpreter.m
//  PompaDroid2.0
//
//  Created by John Watson on 3/8/15.
//  Copyright (c) 2015 John Watson. All rights reserved.
//

#import "InputInterpreter.h"

@implementation InputInterpreter

+(InputIntent)interpretInputIntent:(UIGestureRecognizer *)gestureRecognizer
{
    if( [gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]] )
    {
        return [InputInterpreter interpretTaps:(UITapGestureRecognizer *)gestureRecognizer];
    }
    else if( [gestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]] )
    {
        return [InputInterpreter interpretLongPress:(UILongPressGestureRecognizer *)gestureRecognizer];
    }
    
    return 0;
}

+(InputIntent)interpretTaps:(UITapGestureRecognizer *)gestureRecognizer
{
    if( gestureRecognizer.numberOfTapsRequired == 1 )
    {
        NSLog(@"PUNCH!");
    }
    if( gestureRecognizer.numberOfTapsRequired == 2 )
    {
        NSLog(@"SUPER!");
    }
    
    return 0;
}

+(InputIntent)interpretLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if( gestureRecognizer.numberOfTapsRequired == 0 )
    {
        NSLog(@"WALK!");
    }
    if( gestureRecognizer.numberOfTapsRequired == 1 )
    {
        NSLog(@"RUN!");
    }
    
    return 0;
}

@end
