//
//  InputInterpreter.h
//  PompaDroid2.0
//
//  Created by John Watson on 3/8/15.
//  Copyright (c) 2015 John Watson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"

typedef NS_ENUM(NSUInteger, InputIntent) {
    //Walk
    kInputIntentWalkRight,
    kInputIntentWalkLeft,
    
    //Run
    kInputIntentRunRight,
    kInputIntentRunLeft,
    
    //Punch
    kInputIntentPunchRight,
    kInputIntentPunchLeft,
    
    //Kick
    kInputIntentKickRight,
    kInputIntentKickLeft
};

@interface InputInterpreter : NSObject

+(InputIntent)interpretInputIntent:(UIGestureRecognizer *)gestureRecognizer;

@end
