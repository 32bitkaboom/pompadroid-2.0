//
//  GameViewController.m
//  PompaDroid2.0
//
//  Created by John Watson on 6/29/14.
//  Copyright (c) 2014 John Watson. All rights reserved.
//

#define rgba(redValue, greenValue, blueValue, alphaValue) \
        [UIColor colorWithRed:((float)(redValue))/255.0 \
        green:((float)(greenValue))/255.0 \
        blue:((float)(blueValue))/255.0 \
        alpha:((float)(alphaValue))/1.0]


#import "GameViewController.h"
#import "JSTileMap.h"
#import "GameLevel.h"
#import "PDAxel.h"
#import "sprites.h"
#import "InputLayer.h"
#import "InputInterpreter.h"

@interface GameViewController ()

@property (nonatomic, strong) GameLevel *level;
@property (nonatomic, strong) InputLayer *inputLayer;

@property (nonatomic, strong) PDAxel    *axel;

@property (nonatomic, strong) SKSpriteNode *rightTapZone;
@property (nonatomic, strong) SKSpriteNode *leftTapZone;

@end

@implementation GameViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    // Configure the view.
    SKView * skView = (SKView *)self.view;
    skView.showsFPS = YES;
    skView.showsNodeCount = YES;
    skView.showsDrawCount = YES;
    
    // Create and configure the scene.
    CGSize sceneSize = skView.bounds.size;
    NSLog(@"sceneSize: width=%f   height=%f", sceneSize.width, sceneSize.height);
    
    _level = [GameLevel sceneWithSize:skView.bounds.size];
    [_level loadLevel:PDGameLevel1];
    
    _axel = [[PDAxel alloc] initWithTexture:SPRITES_TEX_AXEL_IDLE_01];
    _axel.position = CGPointMake(100, 100);
    [_level addChild:_axel];
    
    // Present the scene.
    [skView presentScene:self.level];
    
    [self addTapZones];
    [self addInputHandlers];
}

-(void) addInputHandlers
{
    //TAPS
    UITapGestureRecognizer *singleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
    [self.view addGestureRecognizer:singleTapGesture];

    UITapGestureRecognizer *doubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
    doubleTapGesture.numberOfTapsRequired = 2;
    [self.view addGestureRecognizer:doubleTapGesture];

    
    //LONG PRESSES
    UILongPressGestureRecognizer *singleTapLongPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
    singleTapLongPress.numberOfTapsRequired = 0;
    [self.view addGestureRecognizer:singleTapLongPress];
    
    UILongPressGestureRecognizer *doubleTapLongPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
    doubleTapLongPress.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:doubleTapLongPress];
    
    
    //SWIPES
}

-(void) addTapZones
{
    self.leftTapZone = [SKSpriteNode spriteNodeWithColor:rgba(255.0, 0.0, 0.0, 0.6) size:CGSizeMake(100, 768)];
    self.leftTapZone.anchorPoint = CGPointMake(0, 0);
    self.leftTapZone.position = CGPointMake(0, 0);
    
    self.rightTapZone = [SKSpriteNode spriteNodeWithColor:rgba(255.0, 0.0, 0.0, 0.6) size:CGSizeMake(100, 768)];
    self.rightTapZone.anchorPoint = CGPointMake(0, 0);
    self.rightTapZone.position = CGPointMake([self screenDimensions].width - self.rightTapZone.size.width, 0.0);
    
    
    [self.level addChild:self.leftTapZone];
    [self.level addChild:self.rightTapZone];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}


- (void) handleGesture:(UIGestureRecognizer*)gestureRecognizer
{
    CGPoint touchLocation = [gestureRecognizer locationInView:self.view];
    NSLog(@"Got touch at location (%f, %f)", touchLocation.x, touchLocation.y);
    
    [InputInterpreter interpretInputIntent:gestureRecognizer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (CGSize) screenDimensions
{
    return [UIScreen mainScreen].bounds.size;
}

@end
