//
//  PDAxel.h
//  PompaDroid2.0
//
//  Created by John Watson on 7/27/14.
//  Copyright (c) 2014 John Watson. All rights reserved.
//

#import "PDActionCharacter.h"

@interface PDAxel : PDActionCharacter

-(void) performIdle;
-(void) performSuper;

@end
